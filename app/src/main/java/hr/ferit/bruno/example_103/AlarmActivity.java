package hr.ferit.bruno.example_103;

import android.app.Activity;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AlarmActivity extends Activity {

    public static final String KEY_ALARM = "alarm";

    @BindView(R.id.bStopAlarm) Button bStopAlarm;
    Ringtone mRingtone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);
        ButterKnife.bind(this);

        if(getIntent().hasExtra(KEY_ALARM)){
            if(getIntent().getBooleanExtra(KEY_ALARM,false)){
                Uri alarmRingtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
                this.mRingtone = RingtoneManager.getRingtone(this, alarmRingtoneUri);
                this.mRingtone.play();
            }
        }
    }


    @OnClick(R.id.bStopAlarm)
    public void stopAlarm(){
        if(this.mRingtone.isPlaying()){
            Log.d("Example_103", "in stopAlarm");
            this.mRingtone.stop();
            this.finish();
        }
    }
}
