package hr.ferit.bruno.example_103;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TimePicker;
import android.widget.ToggleButton;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity {

    private static final int REQUEST = 10;
    @BindView(R.id.tpAlarmTime) TimePicker tpAlarmTime;
    @BindView(R.id.bSetAlarm) Button bSetAlarm;

    AlarmManager mAlarmManager;
    PendingIntent mPendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        this.mAlarmManager = (AlarmManager) this.getSystemService(ALARM_SERVICE);
    }

    @OnClick(R.id.bSetAlarm)
    public void onSetAlarmClick(){
            setAlarm();
    }

    private void setAlarm() {
        Log.d("Example_103", "in setAlarm()");
        Intent intent = new Intent(this, AlarmReciever.class);
        mPendingIntent = PendingIntent.getBroadcast(
                this, REQUEST, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar calendar = Calendar.getInstance();

        // Do tasks specific to the version:
        if(Build.VERSION.SDK_INT >= 23){
            calendar.set(Calendar.HOUR_OF_DAY, this.tpAlarmTime.getHour());
            calendar.set(Calendar.MINUTE, this.tpAlarmTime.getMinute());
        }else{
            calendar.set(Calendar.HOUR_OF_DAY, this.tpAlarmTime.getCurrentHour());
            calendar.set(Calendar.MINUTE, this.tpAlarmTime.getCurrentMinute());
        }

        long alarmTimeInMillis = calendar.getTimeInMillis();

        this.mAlarmManager.set(AlarmManager.RTC_WAKEUP, alarmTimeInMillis, this.mPendingIntent);
    }
}
