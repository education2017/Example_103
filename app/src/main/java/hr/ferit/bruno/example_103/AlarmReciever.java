package hr.ferit.bruno.example_103;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

public class AlarmReciever extends WakefulBroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("Example_103", "Alarm sounded.");
        Intent startAlarmActivity = new Intent(context, AlarmActivity.class);
        startAlarmActivity.putExtra(AlarmActivity.KEY_ALARM, true);
        context.startActivity(startAlarmActivity);
    }
}
